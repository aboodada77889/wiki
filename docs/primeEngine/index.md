# About

PrimeEngine is an custom built game engine for Luxoral Prime. The engine uses
the [ECS]([entity]-[component]-[system]) architecture to optimize performance
and easy of use. Originally a traditional game object and component inheritance
system was used but this turned out to be slow and messy which led to the
complete redesign of the engine core.

## ECS

[ECS] is the game engine architecture of PrimeEngine. The architecture separates
the behavior ([system]) from the data ([component]) making it more consistent
and eliminating spaghetti code when compared to the traditional game object
inheritance system.

## Future

In the future PrimeEngine will implement the following:

-   A WebGL rendering system
-   Multi-threading using WebWorkers
-   Better entity querying
-   Entity constructor component initialization
-   Scene manager

[ecs]: https://en.wikipedia.org/wiki/Entity_component_system
[entity]: component.md
[component]: component.md
[system]: system.md
