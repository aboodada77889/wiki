# Component

In [PrimeEngine][primeengine] a component is a class that extends the abstract
`Component` class. This class should only contain properties, their initial
values and a constructor if needed. Most of the time a component will be
referred to using a reference to the components constructor and will only be a
container for the properties it contains.

## Example

The simplest possible component is an empty component used to tag
[entities][entity]:

```ts
export class Tag extends Component {}
```

While a more advanced component could hold the properties for a rectangle:

```ts
export class Cube extends Component {
    public length = 0;
    public width = 0;
    public height = 0;
}
```

[primeengine]: index.md
[entity]: component.md
[system]: system.md
