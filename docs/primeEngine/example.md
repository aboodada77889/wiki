# Example

The full example of the things mentioned in [Entity][entity],
[Component][component], [System][system] and [Scene][scene]:

```ts
import {
    Component,
    Engine,
    Entity,
    Rectangle,
    Renderable,
    Scene,
    System,
    Transform
} from "PrimeEngine.ts";

class PleaseRotate extends Component {}

class RotatorSystem extends System {
    private angle: number = 0;

    public readonly queries = { Rotatable: [Transform, PleaseRotate] };

    public update(query: string, entities: Entity[], delta: number): void {
        this.angle += delta;

        if (this.angle > Math.PI * 2) this.angle = 0;

        for (const entity of entities) {
            entity.components.get(Transform).rotation = this.angle;
        }
    }
}

async function main(): Promise<void> {
    const canvas: HTMLCanvasElement = document.getElementById(
        "canvas"
    ) as HTMLCanvasElement;

    const rectangle = new Entity("rectangle", [
        Renderable,
        Transform,
        Rectangle,
        PleaseRotate
    ]);

    rectangle.components.get(Rectangle).width = 100;
    rectangle.components.get(Rectangle).height = 100;

    rectangle.components.get(Renderable).fill.enabled = true;

    const MainScene = new Scene(
        "MainScene",
        [rectangle],
        [new CanvasRenderingSystem(canvas), new RotatorSystem()]
    );

    const engine = new Engine({
        scene: MainScene
    });
    engine.start();
}

main();
```

[entity]: component.md
[component]: component.md
[system]: system.md
[scene]: scene.md
