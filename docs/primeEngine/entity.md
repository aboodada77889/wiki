# Entity

An `Entity` is a container for multiple [components][component] and a `name`
property. This is the basic game object of the [ECS] architecture and is what
the [systems][system] process. The `Entity` class is the basic object in the
engine and is in most cases adequate on its own.

## Example

A basic `Entity` for a `Renderable` `Rectangle` could look something like:

```ts
const rectangle = new Entity("rectangle", [Renderable, Transform, Rectangle]);
```

This entities [components][component] could then be changed:

```ts
rectangle.components.get(Rectangle).width = 10;
rectangle.components.get(Rectangle).height = 10;
```

[ecs]: https://en.wikipedia.org/wiki/Entity_component_system
[primeengine]: index.md
[component]: component.md
[system]: system.md
