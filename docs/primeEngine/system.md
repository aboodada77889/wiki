# System

A system in [PrimeEngine][primeengine] is a class that extends the abstract
`System` class. Systems must implement the `queries` property which contain the
different [components][component] that will be queried for each query. The
system can also extend the methods `preStart`, `start`, `postStart` as well as
`preUpdate`, `update`, `postUpdate`. These methods are called when the
[`Scene`][scene] starts and updates. The pre and post methods does not call with
a specific query name but with all the queried [entities][entity] while `start`
and `update` is called multiple times, once for each matching query. When
`start` and `update` is called they are called with the parameters for the query
that matched and all of the matching entities for that specific query.
`preUpdate`, `update`, `postUpdate` are also called with a parameter for the
current [delta time][delta] of the [`Scene`][scene] update.

## Example

A simple system for rotating [entities][entity]:

```ts
export class RotatorSystem extends System {
    private angle: number = 0;

    public readonly queries = { Rotatable: [Transform] };

    public update(query: string, entities: Entity[], delta: number): void {
        this.angle += delta;

        if (this.angle > Math.PI * 2) this.angle = 0;

        for (const entity of entities) {
            entity.components.get(Transform).rotation = this.angle;
        }
    }
}
```

This system queries for all [entities][entity] with the `Transform`
[`Component`][component]. Each update when all of the [entities][entity]
matching the query are found the `update` method is called. In this case the
`query` parameter is `Rotatable` because the matched queries name is
`Rotatable`. The `entities` parameter contains all of the matched
[entities][entity] in an array.

In the `update` method of the system the systems internal property `angle` is
changed by the [delta time][delta]. The system then resets the `angle` property
to zero if its rotated 360°. After that the system loops over each
[`Entity`][entity] that matched changing its `Transform` rotation to the
`angle`.

[primeengine]: index.md
[component]: component.md
[entity]: entity.md
[scene]: scene.md
[delta]: https://en.wikipedia.org/wiki/Delta_timing
