# Nudiray

<img src="../nudiray.png" width=150 align="left"></img> Despite having no bones,
they have a plethora of specialized muscles in their mouths that allow them to
crush creatures with tough shells, allowing them to eat the meat inside. Most
species do not hunt them due to their toxic meat and blood, which causes most
species to cough them up after eating their meat. However, this species
reproduces via mitosis, splitting into new ones when killed, allowing for them
to live relatively peaceful lives.
