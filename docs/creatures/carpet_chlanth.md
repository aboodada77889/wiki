# Carpet Chlanth

<img src="../carpet_chlanth.png" width=150 align="left"></img> This large
chlanth is the oddest of the species. Unlike most other relatives, their legs
have become huge and have a thin webbing that connects them. They swim over the
reefs searching for corals that are toxic to eat to many other species, making
them toxic to eat to most species. When outside of water, they turn to mush due
to the sacrifice of stability over mobility. When threatened, they leave a long
trail of ink that gets caught in the gills of most species, suffocating them.
