# Clown Sniffler

<img src="../clown_sniffler.png" width=150 align="left"></img> Much to many
scientists surprise, this creature looks amazingly like A. ocellaris, but
filling an entirely different role. Its stripped body allows it hide and live in
a specialized coral. They spend most of their time sifting sand in search of
food. Clown Snifflers seem to have a symbiotic bond with a specific coral
refered to by on board Xenobiologists as Ocellaris Anemone. Ocellaris is a
hostile flora, slapping nearby creatures and consuming smaller ones. However,
Clown Snifflers emit a pheramone that allows them to go undected by the anemone
for there entire lives. This results in rocky burrows with one on top to be
filled to the brim with snifflers.
