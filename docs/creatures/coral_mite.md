# Coral Mite

<img src="../coral_mite.png" width=150 align="left"></img> These ravenous mites
are the clean up crew of the reef, eating all dead things or dying things. They
will even finish off corals that are sick. Coral mites even will go after some
of the larger reef creatures. While info is limited, a type of rare algae seems
to turn these creatures into a ravenous state beyond comprehension. If this
algae ever blooms, serious ecological events would be felt…..
