# Moreinagi

<img src="../moreinagi.png" width=150 align="left"></img> These dangerous
creatures live on the edges of reefs, eating anything that wanders too far from
the biome. They live in groups of 2-4, competing with other groups. Some groups
will scavenge the open waters with some even living in the drifting isles.
